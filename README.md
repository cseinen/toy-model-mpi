toy-model-mpi
=============

Overview
--------

A set of simple *husks* of an atmosphere, coupler and ocean model, to test the creation, splitting and sharing of mpi communicators and simple exchanges.

Compilation
-----------

Can be compiled via `make`. The fortran compiler and FFLAGS should be set in the Makefile. Whether to use cancpl or not should also be set in `atm.f90`.

Usage
-----

The `do_chatter` logical in `atm` and `ocn` switches whether actual passing of strings is done between groups and printed.

1. Atmosphere only (full MPI_COMM_WORLD)

   Set `use_cancpl=0` in atm.f90 (RPN mode, with no changes due to coupler) or `use_cancpl=1` will also work. Compile. Run the `atm.exe` only, e.g.

   ```
   aprun -n 6 ./atm.exe
   ```

2. Coupled atm-cpl(-ocn) using MPMD mode and split MPI_COMM_WORLD

   Set `use_cancpl=1` in atm.f90. Compile. Run in MPMD mode, e.g:

   ```
   aprun -n 6 ./atm.exe : -n 1 ./cpl.exe
   ```

An interactive run
------------------

```
qsub -I -lselect=3:vntype=cray_compute -l place=scatter -l walltime=03:00:00
aprun -n 6 ./atm.exe : -n 1 ./cpl.exe : -n 4 ./ocn.exe
```