module com_cpl

    use mpi

    implicit none

    public :: define_group
    public :: impi

    integer, parameter :: impi = 4

    !> nbuf is the size of the integer array that is prepended
  !! to every MPI data transmission done below
  !! In the past this has always been the 8 word ibuf array
  !! but this could change in the future
  integer(kind=impi), parameter :: nbuf=8

  ! The master task associated with each component model
  ! The *_master variables indicate the rank in MPI_COMM_WORLD not the group rank
  integer(kind=impi), public, save :: cpl_master=-1 !< Master rank in the MPI_COMM_WORLD for the coupler 
  integer(kind=impi), public, save :: atm_master=-1 !< Master rank in the MPI_COMM_WORLD for the atmosphere 
  integer(kind=impi), public, save :: ocn_master=-1 !< Master rank in the MPI_COMM_WORLD for the ocean 
  integer(kind=impi), public, save :: ice_master=-1 !< Master rank in the MPI_COMM_WORLD for the ice model 

  !> Define the task group properties 
  type task_info_t
    integer(kind=4) :: master !< Rank rank within this group
    character(32)   :: group_name !< The name of the group
    integer(kind=4) :: group_min_rank !< The smallest rank id in the group
    integer(kind=4) :: group_max_rank !< The largest rank id in the group
  end type task_info_t

  !> A list of all groups defined in the coupler
  type(task_info_t), pointer, save :: task_info(:)

  !> model_group_comm is the group intra-communicator returned by mpi_comm_split
  integer(kind=impi), public, save :: model_group_comm = -1

  character(len=8) :: member(0:1000)
   !> True if the MPI communicator has been initialized
  logical, save :: com_mpi_initialized = .false.

  integer(kind=impi) :: ierr


 contains

    subroutine define_group(group_name, group_comm, group_member)

        !--- A name used to identify this group
        !--- Only the first 3 characters are used
        !--- Valid values are : cpl, atm, ocn, ice
        character(*), intent(in) :: group_name

        !--- The mpi communicator that is assigned to this group
        integer(kind=impi), intent(inout) :: group_comm

        character(len=8), intent(out), optional :: group_member(0:1000)

        !--- local
        integer :: cpl_rank_min, cpl_rank_max
        integer :: atm_rank_min, atm_rank_max
        integer :: ocn_rank_min, ocn_rank_max
        integer :: ice_rank_min, ice_rank_max
        integer :: ivar
        integer :: verbose=0
        character(len=3) :: name
        integer(kind=impi) :: rank, nproc, proc, color, key

        if ( .not. com_mpi_initialized ) call com_mpi_init()
        call mpi_barrier(MPI_COMM_WORLD, ierr)

        if ( group_name(1:3).ne.'cpl' .and. &
            group_name(1:3).ne.'atm' .and. &
            group_name(1:3).ne.'ocn' .and. &
            group_name(1:3).ne.'ice' ) then
        write(6,'(2a)') 'define_group: Invalid group name ',group_name(1:3)
        write(6,'(a)')  'define_group: Valid names are: atm cpl ocn ice'
        call flush(6)
        call err_exit('define_group',-1)
        endif

        !---Determine the rank of the calling process in MPI_COMM_WORLD
        call mpi_comm_rank ( MPI_COMM_WORLD, rank, ierr )

        !---Determine the number of processes in MPI_COMM_WORLD
        call mpi_comm_size ( MPI_COMM_WORLD, nproc, ierr )

        !--- Allocate the task info array for the current process
        if ( associated(task_info) ) nullify(task_info)
        allocate( task_info(0:nproc-1) )

        if (verbose > 1) then
        write(6,'(4a,3i4)') &
            'define_group: group_name=',group_name(1:3), &
            '  MPI_COMM_WORLD,rank,size ',MPI_COMM_WORLD,rank,nproc
        call flush(6)
        endif

        atm_rank_min = nproc
        atm_rank_max = 0
        ocn_rank_min = nproc
        ocn_rank_max = 0
        cpl_rank_min = nproc
        cpl_rank_max = 0
        ice_rank_min = nproc
        ice_rank_max = 0
        key = 1

        !--- Broadcast the name assigned to each process to MPI_COMM_WORLD and
        !--- use the responses to determine process ranges for each sub group
        do proc=0,nproc-1
        !--- Default integer ivar is required for the min/max functions below
        ivar = proc

        if (proc.eq.rank) then
            !--- Broadcast the current group name (passed in by caller)
            name = group_name(1:3)
        else
            name = 'XXX'
        endif

        call mpi_bcast(name, 3_impi, MPI_CHARACTER, proc, MPI_COMM_WORLD, ierr)

        select case( trim(adjustl(name)) )
            case ('atm')
            atm_rank_min = min(atm_rank_min, ivar)
            atm_rank_max = max(atm_rank_max, ivar)
            case ('ocn')
            ocn_rank_min = min(ocn_rank_min, ivar)
            ocn_rank_max = max(ocn_rank_max, ivar)
            case ('cpl')
            cpl_rank_min = min(cpl_rank_min, ivar)
            cpl_rank_max = max(cpl_rank_max, ivar)
            case ('ice')
            ice_rank_min = min(ice_rank_min, ivar)
            ice_rank_max = max(ice_rank_max, ivar)
            case default
            !--- This should never happen, but ...
            write(6,'(4a)')'define_group: group name ',group_name(1:3),'  Invalid name ',name
            write(6,'(a)') 'define_group: Valid names are: atm cpl ocn ice'
            call err_exit('define_group',-2)
        end select

        enddo

        if (verbose > 1) then
        ! call mpi_barrier(MPI_COMM_WORLD, ierr)
        write(6,'(2a,4(a,i3,a,i3))') &
            'define_group: group_name=',group_name(1:3), &
            '  cpl_rank_min=',cpl_rank_min,' cpl_rank_max=',cpl_rank_max, &
            '  atm_rank_min=',atm_rank_min,' atm_rank_max=',atm_rank_max, &
            '  ocn_rank_min=',ocn_rank_min,' ocn_rank_max=',ocn_rank_max, &
            '  ice_rank_min=',ice_rank_min,' ice_rank_max=',ice_rank_max
        call flush(6)
        endif

        !--- Split MPI_COMM_WORLD into sub groups, one group for each of cpl, atm, ocn, ice
        color = MPI_UNDEFINED
        if ((rank.ge.cpl_rank_min).and.(rank.le.cpl_rank_max)) then
        !--- coupler
        color = 1
        endif
        if ((rank.ge.atm_rank_min).and.(rank.le.atm_rank_max)) then
        !--- atmosphere
        color = 2
        endif
        if ((rank.ge.ocn_rank_min).and.(rank.le.ocn_rank_max)) then
        !--- ocean
        color = 3
        endif
        if ((rank.ge.ice_rank_min).and.(rank.le.ice_rank_max)) then
        !--- sea ice
        color = 4
        endif

        !--- Build intra-communicator (group_comm) for local sub-groups
        group_comm = MPI_UNDEFINED
        call mpi_comm_split(MPI_COMM_WORLD,color,key,group_comm,ierr)

        !--- The group communicator returned by mpi_comm_split will have the same value
        !--- for every process involved (ie every process in MPI_COMM_WORLD)
        !--- Use this group communicator for collective operations that are to be
        !--- isolated to a particular group
        !--- Save the value of the group communicator for use elswhere
        model_group_comm = group_comm
        if ( verbose > 2 ) then
        write(6,*)"define_group: Task ",rank,"  group_name=",trim(group_name),"  group_comm=",group_comm
        call flush(6)
        endif

        !--- Assign the master task associated with each group
        !--- The master task is always the task with the lowest rank
        !--- Note that if a group is not used (e.g. no ocean) then the master task
        !--- associated with that group will be nproc, which is outside the range of
        !--- mpi tasks in MPI_COMM_WORLD and therefore cannot be used without error
        cpl_master = cpl_rank_min
        atm_master = atm_rank_min
        ocn_master = ocn_rank_min
        ice_master = ice_rank_min

        !--- Assign a complete copy of the task info array for each process
        do proc=0,nproc-1
        if ((proc.le.cpl_rank_max).and.(proc.ge.cpl_rank_min)) then
        task_info(proc)%group_name     = "cpl"
        task_info(proc)%group_min_rank = cpl_rank_min
        task_info(proc)%group_max_rank = cpl_rank_max
        task_info(proc)%master         = task_info(proc)%group_min_rank
        endif
        if ((proc.le.atm_rank_max).and.(proc.ge.atm_rank_min)) then
        task_info(proc)%group_name     = "atm"
        task_info(proc)%group_min_rank = atm_rank_min
        task_info(proc)%group_max_rank = atm_rank_max
        task_info(proc)%master         = task_info(proc)%group_min_rank
        endif
        if ((proc.le.ocn_rank_max).and.(proc.ge.ocn_rank_min)) then
        task_info(proc)%group_name     = "ocn"
        task_info(proc)%group_min_rank = ocn_rank_min
        task_info(proc)%group_max_rank = ocn_rank_max
        task_info(proc)%master         = task_info(proc)%group_min_rank
        endif
        if ((proc.le.ice_rank_max).and.(proc.ge.ice_rank_min)) then
        task_info(proc)%group_name     = "ice"
        task_info(proc)%group_min_rank = ice_rank_min
        task_info(proc)%group_max_rank = ice_rank_max
        task_info(proc)%master         = task_info(proc)%group_min_rank
        endif
        enddo

        !--- Assign the member array
        !--- Depreciated. This is only used with send_fld and recv_fld
        do proc=0,nproc-1
        if ((proc.le.cpl_rank_max).and.(proc.ge.cpl_rank_min)) member(proc)='coupler '
        if ((proc.le.atm_rank_max).and.(proc.ge.atm_rank_min)) member(proc)='atmos   '
        if ((proc.le.ocn_rank_max).and.(proc.ge.ocn_rank_min)) member(proc)='ocean   '
        enddo

        if ( present(group_member) ) then
        group_member(:) = member(:)
        endif

        !--- Wait for all mpi communications to complete
        call mpi_barrier(MPI_COMM_WORLD, ierr)

        !--- Ensure all processes have arrived before proceeding
        if (verbose.gt.2) then
        write(6,*)"define_group: Task ",rank," waiting for all."
        call flush(6)
        endif
        call mpi_barrier(MPI_COMM_WORLD, ierr)
        if (verbose.gt.2) then
        write(6,*)"define_group: Task ",rank," moving on now."
        call flush(6)
        endif

        if ( verbose > 1 ) then
        write(6,*)"define_group: ",trim(group_name)," group_comm,rank: ",group_comm,rank
        call flush(6)
        endif

        if ( verbose > 2 ) then
        do proc=0,nproc-1
            write(6,*)"define_group: rank=",rank,"  name=",trim(task_info(proc)%group_name), &
            "  min_rank, max_rank, master: ", task_info(proc)%group_min_rank, &
            task_info(proc)%group_max_rank, task_info(proc)%master
        enddo
        call flush(6)
        endif

    end subroutine define_group

    subroutine err_exit(name,n)
        implicit none
  
        character(*) :: name
        integer, intent(in) :: n
  
        integer :: iou, lename
        integer(4) :: myrank, ierr, n4
  
        character(80), parameter :: dash = &
        '--------------------------------------------------------------------------------'
        character(80), parameter :: star = &
        '********************************************************************************'
  
        call mpi_comm_rank ( MPI_COMM_WORLD, myrank, ierr )

        if (myrank.eq.0 .or. (n.lt.0 .and. n.ge.-100) ) then
          lename=len_trim(name)
          if (lename.lt.1) lename=1
  
          if(n.ge.0) write(6,'(a,"  END  ",a,1x,a,i8)') &
            dash(1:8),name(1:lename),dash(1:81-lename),n
  
          if(n.lt.0) write(6,'(a,"  END  ",a,1x,a,i8)') &
            star(1:8),name(1:lename),star(1:81-lename),n
  
        endif
        if (myrank.eq.0) then
         print *, '0closing data i/o units'
         do iou=1,100
          if (iou.ne.5.and.iou.ne.6) close (iou)
         enddo
        endif
        call flush(6)
  
        if ( n.ge.0 .or. n.lt.-100 ) then
          call system('sleep 3')
          print *, '0got to before mpi_finalize'
          call flush(6)
          call mpi_finalize(ierr)
          print *, '0got to after mpi_finalize'
          call flush(6)
          stop
        else
          n4=n
          call system('sleep 3')
          call mpi_abort(model_group_comm,n4,ierr)
          call abort
        endif
      end subroutine err_exit

      subroutine com_mpi_init()
        integer(kind=impi) :: myrank, ierr
        integer :: verbose=0
        logical(kind=impi) :: mpi_init_was_called
  
        !--- Do nothing if already initialized
        if ( com_mpi_initialized ) return
  
        !--- Call MPI_init if MPI is not already initialized
        !--- The atm and the coupler will have already called mpi_init
        !--- but NEMO will not call mpi_init when it is coupled
        call mpi_initialized ( mpi_init_was_called, ierr )
        if ( .not. mpi_init_was_called ) then
          call mpi_init( ierr )
        else
          if ( verbose > 0 ) then
            call mpi_comm_rank ( MPI_COMM_WORLD, myrank, ierr )
            write(6,'(a,i4)')'com_mpi_init: ** II ** MPI is already initialized on task ',myrank
            call flush(6)
          endif
        endif
  
        call mpi_comm_rank ( MPI_COMM_WORLD, myrank, ierr )
  
        !--- WARNING ABOUT THE XLF COMPILER AND STRING INITIALIZATION ---
        !--- It seems that the xlf compiler (and perhaps others) cannot initialize character
        !--- variables correctly when those variables are part of an array.
        !--- If the first element in the list is initialized with a string of a given length
        !--- then all other elements in the list will be truncated at that length.
        !--- e.g. if the first element is initialized as "GT" and the second element is
        !--- initialized as "SIC" then the second element will be truncated
        !--- to 2 characters "SI"
        !--- WARNING ABOUT THE XLF COMPILER AND STRING INITIALIZATION ---
  
        if ( verbose > 0 ) then
          write(6,'(a,i4)')"com_mpi_init: ** II ** Initialization complete on task ",myrank
          call flush(6)
        endif
  
        !--- Set a logical flag to indicate that com_cpl has been initialized
        com_mpi_initialized = .true.
  
      end subroutine com_mpi_init
  
end module com_cpl