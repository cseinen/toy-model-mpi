program cpl
    use mpi
    use com_cpl, only: define_group

    implicit none
    integer ierror, size, rank, i, cpl_local_comm, color, key
    character (len=128) message
    
    call MPI_INIT(ierror)
    color=2
    !call mpi_comm_split(MPI_COMM_WORLD, color, key, cpl_local_comm, ierror)
    call define_group('cpl', cpl_local_comm)

    call MPI_COMM_SIZE(cpl_local_comm, size, ierror)
    call MPI_COMM_RANK(cpl_local_comm, rank, ierror)
  
    write(*,*) 'CPL :', rank, ' of ', size  
    call flush(6)
    call MPI_FINALIZE(ierror)


end program cpl