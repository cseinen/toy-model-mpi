program atm
    use mpi
    use com_cpl, only: define_group

    implicit none
    integer ierror, size, rank, i, atm_local_comm, color, key, internal_comm_odd, internal_comm_even
    integer split
    character (len=128) message
    integer, parameter :: use_cancpl = 1 
    logical, parameter :: do_chatter = .TRUE.    

    atm_local_comm = MPI_UNDEFINED
    internal_comm_odd = MPI_UNDEFINED
    internal_comm_even = MPI_UNDEFINED

    call MPI_INIT(ierror)

   if (use_cancpl) then
       call define_group('atm', atm_local_comm)
    else
        atm_local_comm = MPI_COMM_WORLD
    endif

    call MPI_COMM_SIZE(atm_local_comm, size, ierror)
    call MPI_COMM_RANK(atm_local_comm, rank, ierror)

    ! From the full ATM group
    call MPI_COMM_SIZE(atm_local_comm, size, ierror)
    call MPI_COMM_RANK(atm_local_comm, rank, ierror)
    write(*,*) 'ATM:', rank, ' of ', size
    call flush(6)

    if (do_chatter) then
        if(rank == 0) then
            message = "You are all equal to me"
            write(6,*) "ATM all lead communicates: ", trim(message)
            call flush(6)
            do i = 1, size -1
               call MPI_SEND(message, 128, MPI_CHAR, i, 1, atm_local_comm, ierror)
            end do
        else
            call MPI_RECV(message, 128, MPI_CHAR, 0, 1, atm_local_comm, MPI_STATUS_IGNORE, ierror)
            write(6,*) "ATM all rank", rank, " of ",  size," gets the command: ", trim(message)
            call flush(6)
        end if
        call MPI_BARRIER( atm_local_comm, ierror)
    endif


    ! Split the atm communicator into two groups, based on odd or even rank
    split  = mod(rank, 2)

    if (split == 0) then
        color = 1
        call atm_comm_split(atm_local_comm, color, key, internal_comm_even, ierror)
    else
        color = 2
        call atm_comm_split(atm_local_comm, color, key, internal_comm_odd, ierror)
    endif

    !From the ODD / EVEN groups
    if (split == 0) then
        call MPI_COMM_SIZE(internal_comm_even, size, ierror)
        call MPI_COMM_RANK(internal_comm_even, rank, ierror)
        write(*,*) 'ATM EVEN:', rank, ' of ', size
        call flush(6)

        if (do_chatter) then
            if(rank == 0) then
                message = "hello your eveness"
                write(6,*) "ATM EVEN lead communicates: ", trim(message)
                call flush(6)
                do i = 1, size -1
                   call MPI_SEND(message, 128, MPI_CHAR, i, 1, internal_comm_even, ierror)
                end do
            else
                call MPI_RECV(message, 128, MPI_CHAR, 0, 1, internal_comm_even, MPI_STATUS_IGNORE, ierror)
                write(6,*) "ATM EVEN rank", rank, " of ",  size," gets the command: ", trim(message)
                call flush(6)
            end if
        endif
    else
        call MPI_COMM_SIZE(internal_comm_odd, size, ierror)
        call MPI_COMM_RANK(internal_comm_odd, rank, ierror)
        write(*,*) 'ATM ODD:', rank, ' of ', size
        call flush(6)

        if (do_chatter) then
            if(rank == 0) then
                message = "You are an odd duck"
                write(6,*) "ATM ODD lead communicates: ", trim(message)
                call flush(6)
                do i = 1, size -1
                   call MPI_SEND(message, 128, MPI_CHAR, i, 1, internal_comm_odd, ierror)
                end do
            else
                call MPI_RECV(message, 128, MPI_CHAR, 0, 1, internal_comm_odd, MPI_STATUS_IGNORE, ierror)
                write(6,*) "ATM ODD rank", rank, " of ",  size," gets the command: ", trim(message)
                call flush(6)
            end if
        endif
    endif
    
    call MPI_FINALIZE(ierror)

end program atm

subroutine atm_comm_split(old_comm, color, key, new_comm, ierror)
    integer, intent(in) :: old_comm, color, ierror, key
    integer, intent(inout) :: new_comm

    call mpi_comm_split(old_comm, color, key, new_comm, ierror)

end subroutine atm_comm_split